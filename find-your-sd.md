---
layout: page
title: How to find your SD card's device
date: 2020-04-21 11:16:36 -0500
permalink: /find-your-sd/
---

In laptop computers with SD card slots, this will often be
`/dev/mmcblk0`. In most desktop computers, even if it has an
integrated SD card reader, they will be connected by a multiport USB
unit and will be presented as <code
class="highlighter-rouge">/dev/sd<em>X</em></code>, where X is a
single letter. Given your first hard drive is often `/dev/sda`, you
will be looking for the highest letter possible.

I find the following recipe to be reliable and simple:

1. Open a terminal
2. Get the list of existing devices in your system into a temporary
   file (say, `/tmp/before_sd`):

		$ ls /dev > /tmp/before_sd
3. Insert your SD card (be it via an external reader or directly to
   the SD port of your computer)
4. Get again the list of devices into a second temporary file (say,
   `/tmp/after_sd`):

		$ ls /dev > /tmp/after_sd

5. Compare them. Don't worry, Unix is your friend!

		$ diff /tmp/before_sd /tmp/after_sd
		36a37,39
		> mmcblk0
		> mmcblk0p1
		> mmcblk0p2

	Or, if the device follows `sdX`, it would be:
	
		$ diff /tmp/before_sd /tmp/after_sd
		55a56,57
		> sdc
		> sdc1
	
	Note the _topmost name_ you received — In my case, either
    `mmcblk0` or `sdc`. That's the device you want to write to! (the
    other devices will point to _partitions_ within the media you
    inserted).
	
6. Just for cleanness sake, remove the temporary files:

		$ rm /tmp/before_sd /tmp/after_sd
